angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'ionic.contrib.ui.cards', 'angularMoment', 'ui.gravatar', 'emoticonizeFilter', 'zumba.angular-waypoints', 'LocalForageModule', 'ngRoute', 'ngSanitize', 'linkify'])

.run(['$ionicPlatform', '$rootScope', '$state', '$http', '$ionicPopup', 'accountServices', '$location', '$anchorScroll', '$sce', '$ionicPopover', '$q', '$localForage', '$sanitize', 'linkify', function($ionicPlatform, $rootScope, $state, $http, $ionicPopup, accountServices, $location, $anchorScroll, $sce, $ionicPopover, $q, $localForage, $sanitize, linkify) {
  $rootScope.rippled = [ 'wss://s1.ripple.com:443', 'wss://rippled.providence.solutions:443' ];
  $rootScope.receiveName = 'OurProvidence';
  $rootScope.receiveAddress = 'rEoazhPt4VJuSs6yB5irTVitjmwsXuqwyN';
  $rootScope.rippleTxtUrl = 'https://pledger.club/ripple.txt';
  $rootScope.domain = 'pledger.club';

  $rootScope.info = {};
  $rootScope.info.backup = true;

  $rootScope.signatures = {};
  $rootScope.signatures.signatures = [];

  $rootScope.chatData = {};
  $rootScope.chatData.messages = [];

  $rootScope.loggedInAccount = {};
  $rootScope.balances = {};

  $rootScope.packet = {};

  $rootScope.pledge = {};

  $rootScope.sectioned;

  var Remote = ripple.Remote;
  $rootScope.remote = new Remote({
	servers: $rootScope.rippled
  });

  $rootScope.globalRemote = new Remote({
	servers: $rootScope.rippled
  });

  $localForage.getItem('lastTransactions').then(function(data) {
  	if (!data){
		$localForage.setItem('lastTransactions', JSON.stringify([])).then(function(data) {

		});
	}
  });

  if ($location.path() == '/tab/pledge'){
	  $rootScope.sectioned = 'pledge';

  }
  else if ($location.path() == '/tab/pledger'){
	  $rootScope.sectioned = 'home';
  }
  else if ($location.path() == '/tab/mypledges'){
	  $rootScope.sectioned = 'myPledges';
  }

  $rootScope.newPledge = function(){
	  var defer = $q.defer();
	  $rootScope.info.signature = $rootScope.signatures.signaturePad.toDataURL();
	  if ($rootScope.info.signature && $rootScope.info.name && $rootScope.loggedInAccount.publicKey && accountServices.validateEmail($rootScope.info.email)){
		  $rootScope.info.rippleAddress = $rootScope.loggedInAccount.publicKey;
		  $rootScope.info.rippleName = $rootScope.loggedInAccount.rippleName;
		  var today = new Date();
		  $rootScope.info.reportedDate = today.toString();
		  $rootScope.info.permissions = 'public';
		  $rootScope.info.signedPledge = ripple.Message.signMessage($rootScope.info.reportedDate + $rootScope.info.pledge + $rootScope.info.signature, $rootScope.loggedInAccount.secretKey, $rootScope.info.rippleAddress);
		  var rippleFunc = function(){
			  var poster = function(){
				  $http.post('https://invest.providence.solutions/newPledge', angular.copy($rootScope.info)).
					  success(function(data, status, headers, config) {
						  $localForage.getItem('profile').then(function(data) {
							  if (data){
								  var datum = JSON.parse(data);
								  if (datum && datum[0]){
									  var ined = false;
									  datum = _.map(datum, function(dat){
										  if (dat.rippleName == $rootScope.loggedInAccount.rippleName){
											  $rootScope.info.name = dat.name;
											  $rootScope.info.email = dat.email;
											  ined = true;
										  }
										  return dat;
									  });
									  if (!ined){
										  datum.push({rippleName: $rootScope.loggedInAccount.rippleName, publicKey: $rootScope.loggedInAccount.publicKey, name: $rootScope.info.name, email: $rootScope.info.email});
									  }
									  $localForage.setItem('profile', JSON.stringify(datum)).then(function(data) {

									  });
								  }
							  }
							  else {
								  var packet = JSON.stringify([{rippleName: $rootScope.loggedInAccount.rippleName, publicKey: $rootScope.loggedInAccount.publicKey, name: $rootScope.info.name, email: $rootScope.info.email}]);
								  $localForage.setItem('profile', packet).then(function(data) {

								  });
							  }
						  });
						  $rootScope.signatures.signaturesTotal = data.total;
						  $rootScope.info.title = '';
						  $rootScope.info.description = '';
						  $rootScope.info.signature = '';
						  $rootScope.info.rippleAddress = '';
						  $rootScope.info.rippleName = '';
						  $rootScope.info.signedPledge = '';
						  $rootScope.info.backupHash = '';
						  $rootScope.info.reportedDate = '';
						  $rootScope.info.backup = true;
						  defer.resolve(data.pledge);
					  }).
					  error(function(data, status, headers, config) {
						  defer.resolve(false);
					  });
			  }
			  var amount = ripple.Amount.from_human('0.1XRP');
			  $rootScope.remote.setSecret($rootScope.loggedInAccount.publicKey, $rootScope.loggedInAccount.secretKey);

			  var transaction = $rootScope.remote.createTransaction('Payment', {
				  account: $rootScope.loggedInAccount.publicKey,
				  destination: $rootScope.receiveAddress,
				  amount: amount
			  });

			  transaction.addMemo('Pledger Club', JSON.stringify({type : 'pledge', domain : $rootScope.domain, id : $rootScope.info.title, role : 'OP', noHandSignVerifiablePledge : ripple.Message.signMessage($rootScope.info.reportedDate + $rootScope.info.pledge, $rootScope.loggedInAccount.secretKey, $rootScope.info.rippleAddress), signedPledge : $rootScope.info.signedPledge, date : $rootScope.info.reportedDate.toString()}));

			  transaction.addMemo('request', '', $rootScope.loggedInAccount.secretKey, $rootScope.info.rippleAddress);

			  transaction.submit(function(err, res) {
				  if (!err && res && res.tx_json && res.tx_json.hash){
					  $rootScope.info.signedDateTime = ripple.Message.signMessage(res.tx_json.date.toString(), $rootScope.loggedInAccount.secretKey, $rootScope.info.rippleAddress);
					  $rootScope.info.backupHash = res.tx_json.hash;
					  $rootScope.info.hash = res.tx_json.hash;
					  $localForage.getItem('lastTransactions').then(function(data) {
						  var transactionList = JSON.parse(data);
						  var inList;
						  if (transactionList && transactionList[0]){
							  transactionList = _.map(transactionList, function(item){if (item.domain == $rootScope.domain){inList = true; item = {hash : res.tx_json.hash, dateTime: res.tx_json.date.toString(), rippleName: $rootScope.loggedInAccount.rippleName, rippleAddress: $rootScope.loggedInAccount.publicKey}} return item;});
						  }
						  if (!inList){
							  transactionList.push({domain : $rootScope.domain, hash : res.tx_json.hash, dateTime: res.tx_json.date.toString(), rippleName: $rootScope.loggedInAccount.rippleName, rippleAddress: $rootScope.loggedInAccount.publicKey});
						  }
						  $localForage.setItem('lastTransactions', JSON.stringify(transactionList)).then(function() {

						  });
					  });
					  poster();
				  }
			  });
		  }
		  if ($rootScope.remote.isConnected()){
			  rippleFunc();
		  }
		  else {
			  $rootScope.remote.connect(function() {
				  rippleFunc();
			  });
		  }
	  }
	  else {
		  defer.resolve(false);
	  }
	  return defer.promise;
  }

  $rootScope.newPledgePrompt = function(){
	  $rootScope.popover.hide();
	  var myPopup = $ionicPopup.show({
		  template: '<span class="providence-primary" style="position: absolute;text-align: center;left: 50%;top: 35px;margin-left: -7px;" ng-if="loggedInAccount.pledging"><i class="icon ion-loading-c" style="height: 14px;width: 13px;"></i></span><style>.popup { width:321px !important; } .popup-body { padding-bottom: 1px !important; }</style><form><label class="item item-input"><input type="text" placeholder="Title" ng-model="info.title"></label><label class="item item-input"><input type="text" placeholder="Description" ng-model="info.description"></label><label class="item item-input"><textarea type="text" placeholder="Pledge" ng-model="info.pledge"></textarea></label><label class="item item-input"><input type="text" placeholder="Name" ng-model="info.name"></label><label class="item item-input"><input type="email" placeholder="Email" ng-model="info.email"></label><signature data-tap-disabled="true"></signature></form>',
		  title: '<span class="providence-primary ionHead">New Pledge</span>',
		  scope: $rootScope,
		  buttons: [
			  {
				  text: '<span class="providence-primary smallButton">Cancel</span>',
				  type: 'marketing-secondary-bg'
			  },
			  {
				  text: '<span class="providence-primary smallButton">New</span>',
				  type: 'marketing-primary-bg',
				  onTap: function(e) {
					  e.preventDefault();
					  if (!$rootScope.loggedInAccount.pledging){
						  $rootScope.loggedInAccount.pledging = true;
						  $rootScope.newPledge().then(function(val){
							  $rootScope.loggedInAccount.pledging = false;
							  if (val){
								  $location.path('tab/pledge/' + val._id.toString());
								  myPopup.close();
							  }
						  });
					  }
				  }
			  }
		  ]
	  });
  }

  $rootScope.exportSignatures = function(){
	var objectReq = {_id : $rootScope.pledge._id};
	var poster = function(objectReqNew, defer){
		var form = $('<form method="POST" action="' + 'https://invest.providence.solutions/exportSignatures' + '">');
		$.each(objectReqNew, function(k, v) {
			form.append($('<input type="hidden" name="' + k +
				'" value="' + v + '">'));
		});
		$('body').append(form);
		form.submit();
		defer.resolve(true);
	}
	accountServices.noNetNeutrality(objectReq, poster).then(function(){

	});
  }

  $rootScope.section = function(sec){
  	if (sec == 'home'){
		$state.go('tab.pledger');
	}
	else if (sec == 'myPledges'){
		$location.path('tab/mypledges/' + $rootScope.loggedInAccount.rippleName);
	}
	$rootScope.sectioned = sec;
  }

  $ionicPopover.fromTemplateUrl('templates/nav-popover.html', {
	scope: $rootScope
  }).then(function(popover) {
	  $rootScope.popover = popover;
  });

  $rootScope.openPopover = function($event) {
	  if ($rootScope.loggedInAccount && $rootScope.loggedInAccount.publicKey){
		  $rootScope.popover.show($event);
	  }
	  else {
		  $rootScope.connectRipplePrompt().then(function(){
			  $rootScope.popover.show($event);
		  });
	  }
  };

  $rootScope.closePopover = function() {
	$rootScope.popover.hide();
  };

  $rootScope.toggleChat = function(){
	  if ($rootScope.chatData.chatOn){
		  $rootScope.chatData.chatOn = false;
	  }
	  else {
		  $rootScope.chatData.chatOn = true;
		  accountServices.getRootTransactions();
	  }
  }

  $rootScope.$watch('packet.rippleName', _.debounce(function (newValue, oldValue) {
	if (newValue){
		$http.get('https://id.ripple.com/v1/user/' + newValue).
			success(function(data, status, headers, config) {
				if (data && data.address){
					$rootScope.blob = data;
				}
				else {
					$rootScope.blob = '';
				}
			}).
			error(function(data, status, headers, config) {
				$rootScope.blob = '';
			});
	}
	else {
		$rootScope.blob = '';
	}
  }, 500), true);

  try {
	$rootScope.socket = io.connect('https://invest.providence.solutions');
	accountServices.socket();
	accountServices.messageSubscribe();
  }
  catch(err) {
  }

  $rootScope.sendMessage = function(){
	if ($rootScope.chatData.message && $rootScope.loggedInAccount.publicKey){
		$rootScope.chatData.sending = true;
		var amount = ripple.Amount.from_human('0.1XRP');
		var rippleFunc = function(){
			$rootScope.remote.setSecret($rootScope.loggedInAccount.publicKey, $rootScope.loggedInAccount.secretKey);

			var transaction = $rootScope.remote.createTransaction('Payment', {
				account: $rootScope.loggedInAccount.publicKey,
				destination: $rootScope.receiveAddress,
				amount: amount
			});

			transaction.addMemo('Provichat', JSON.stringify({type : 'chatRoom', rooms : ['provichat']}));

			transaction.addMemo('message', $rootScope.chatData.message.toString());

			transaction.submit(function(err, res) {
				$rootScope.$apply(function(){
					if (!err){
						var message = {};
						message.message = $sce.trustAsHtml($sanitize(linkify.twitterRippleProvidence(angular.copy($rootScope.chatData.message))));
						message.date = res.tx_json.date;
						message.hash = res.tx_json.hash;
						message.address = res.tx_json.Account;
						message.username = $rootScope.loggedInAccount.rippleName;
						var inRay = _.find($rootScope.chatData.messages, function(sigged) {return message.hash == sigged.hash;});
						if (!inRay){
							$rootScope.chatData.messages.unshift(message);
						}
					}
					$rootScope.chatData.message = '';
					$rootScope.chatData.sending = false;
				});
			});
		}
		if ($rootScope.remote.isConnected()){
			rippleFunc();
		}
		else {
			$rootScope.remote.connect(function() {
				rippleFunc();
			});
		}
	}
  }

	$rootScope.requestToken = function(){
		var BlobClient = new ripple.VaultClient();
		BlobClient.requestToken($rootScope.loggedInAccount.twoFactor.blob_url, $rootScope.loggedInAccount.twoFactor.blob_id, 1, function(){

		});
	}

	$rootScope.login = function(){
		var defer = $q.defer();
		var VC = new ripple.VaultClient();
		VC.loginAndUnlock($rootScope.packet.rippleName, $rootScope.packet.password, '34535345', function(err,info){
			console.log(err);
			$rootScope.loggedInAccount.connecting = false;
			$rootScope.swirl = false;
			if (err && err.twofactor){
				$rootScope.$apply(function(){
					$rootScope.loggedInAccount.twoFactor = err.twofactor;
				});
			}
			else if (!err && info){
				$rootScope.loggedInAccount = {};
				$rootScope.loggedInAccount.rippleName = info.username;
				$rootScope.loggedInAccount.publicKey = info.blob.data.account_id;
				$rootScope.loggedInAccount.email = info.blob.data.email;
				$rootScope.loggedInAccount.secretKey = info.secret;
				$rootScope.blob.secret = info;
				$localForage.getItem('profile').then(function(data) {
					if (data){
						var datum = JSON.parse(data);
						if (datum && datum[0]){
							datum.forEach(function(dat){
								if (dat.rippleName == $rootScope.loggedInAccount.rippleName){
									$rootScope.info.name = dat.name;
									$rootScope.info.email = dat.email;
								}
							});
						}
					}
				});
				if ($rootScope.pledge && $rootScope.pledge.admins){
					$rootScope.pledge.admins.forEach(function(admin){
						if (admin.rippleAddress ==  $rootScope.loggedInAccount.publicKey){
							$rootScope.admin = admin;
						}
					});
				}
				var rippleFunc = function(){
					accountServices.getInfo();
					$rootScope.remote.requestUnsubscribe(['transactions'], function(){
						var request = $rootScope.remote.requestSubscribe(['transactions']);
						request.setServer($rootScope.rippled);

						$rootScope.remote.on('transaction', function onTransaction(transaction) {
							accountServices.accountInfo(transaction);
							accountServices.messageInfo(transaction);
						});
						request.request();
					});
				}
				if ($rootScope.remote.isConnected()){
					rippleFunc();
				}
				else {
					$rootScope.remote.connect(function() {
						rippleFunc();
					});
				}
				defer.resolve();
			}
		});
		return defer.promise;
	}

	$rootScope.connectRipplePrompt = function(){
		var deferred = $q.defer();
		var myPopup = $ionicPopup.show({
			template: '<span class="providence-primary" style="position: absolute;text-align: center;left: 50%;top: 58px;margin-left: -7px;" ng-if="loggedInAccount.connecting"><i class="icon ion-loading-c" style="height: 14px;width: 13px;"></i></span><input type="text" placeholder="Ripple Name" ng-model="packet.rippleName" style="padding:5px;color: #14274d;"><i ng-if="blob" class="icon ion-checkmark-circled" style="position: absolute;top: 88px;left: 100%;margin-left: -40px;font-size: 20px;background: #fff;width: 30px;padding-left: 10px;color: #14274d;"></i><input type="password" placeholder="Password" style="padding:5px;color: #14274d;" ng-model="packet.password"><input ng-if="loggedInAccount.twoFactor" type="text" placeholder="Token" style="padding:5px;color: #14274d;" ng-model="packet.twofactor"><button ng-click="requestToken()" style="text-align:center;margin-top:10px;" ng-if="loggedInAccount.twoFactor"><span class="providence-primary smallButton marketing-primary-bg">SMS Token</span></button>',
			title: '<span class="providence-primary ionHead">Connect Ripple</span>',
			subTitle: '<a target="_blank" href="https://rippletrade.com" class="providence-primary">Have a Ripple account?</span>',
			scope: $rootScope,
			buttons: [
				{
					text: '<span class="providence-primary smallButton">Cancel</span>',
					type: 'marketing-secondary-bg'
				},
				{
					text: '<span class="providence-primary smallButton">Connect</span>',
					type: 'marketing-primary-bg',
					onTap: function(e) {
						if (!$rootScope.packet.rippleName || !$rootScope.packet.password) {
							e.preventDefault();
						}
						else {
							$rootScope.swirl = true;
							$rootScope.loggedInAccount.connecting = true;
							e.preventDefault();
							if ($rootScope.loggedInAccount.twoFactor){
								var options = {
									url         : $rootScope.loggedInAccount.twoFactor.blob_url,
									id          : $rootScope.loggedInAccount.twoFactor.blob_id,
									device_id   : $rootScope.loggedInAccount.twoFactor.device_id,
									token       : $rootScope.packet.twofactor,
									remember_me : true
								};
								new ripple.VaultClient().verifyToken(options, function(err, resp) {
									if (err) {

									} else {
										$rootScope.login().then(function(){
											deferred.resolve();
											myPopup.close();
										});
									}
								});
								$rootScope.loggedInAccount.twoFactor;
							}
							else {
								$rootScope.login().then(function(){
									deferred.resolve();
									myPopup.close();
								});
							}
						}
					}
				}
			]
		});
		return deferred.promise;
	}

  moment.locale('en', {
	relativeTime : {
		future: "IN %s",
		past:   "%s AGO",
		s:  "SECONDS",
		m:  "ONE MINUTE",
		mm: "%d MINUTES",
		h:  "ONE HOUR",
		hh: "%h HOURS",
		d:  "ONE DAY",
		dd: "%d DAYS",
		M:  "ONE MONTH",
		MM: "%d MONTHS",
		y:  "ONE YEAR",
		yy: "%d YEARS"
	}
  });

  $rootScope.getAvailable = function(){
	  var rippleFunc = function(){
		  $rootScope.remote.request('account_offers', $rootScope.receiveAddress, function(err, info) {
			  if (info && info.offers && info.offers[0]){
				  var offerTotal = 0;
				  info.offers.forEach(function(offer){
					  if (offer.taker_gets.currency == 'PVD'){
						  offerTotal += Number(offer.taker_gets.value);
					  }
				  });
				  $rootScope.sharesAvailable = offerTotal;
			  }
		  });
	  }
	  if ($rootScope.remote.isConnected()){
		  rippleFunc();
	  }
	  else {
		  $rootScope.remote.connect(function() {
			  rippleFunc();
		  });
	  }
  }

  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
}])

.config(['$stateProvider', '$urlRouterProvider', '$compileProvider', function($stateProvider, $urlRouterProvider, $compileProvider) {
  $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|chrome-extension):/);
  $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|ftp|file|blob):|^\s*data:image\//);
  $stateProvider
    .state('tab', {
      url: '/tab',
      abstract: true,
      templateUrl: 'templates/tabs.html'
    })

    .state('tab.pledge', {
	  url: '/pledge/:id',
	  views: {
		  'tab-pledge': {
			  templateUrl: 'templates/tab-pledge.html',
			  controller: 'PledgeCtrl'
		  }
	  },
	  resolve: {
		  getPledge: ['$q', '$stateParams', '$rootScope', 'accountServices', '$http', '$sanitize', 'linkify', '$sce', function($q, $stateParams, $rootScope, accountServices, $http, $sanitize, linkify, $sce){
			  var defer = $q.defer();
			  accountServices.getRippleTxtSettings().then(function(){
				  $http.get('https://invest.providence.solutions/pledge?_id=' + $stateParams.id).success(function(data, status, headers, config) {
					  if (data && data.pledge){
						  if (data.pledge.pledge){
							  data.pledge.pledgeDisplay = $sce.trustAsHtml($sanitize(linkify.twitterRippleProvidence(data.pledge.pledge)));
						  }
						  $rootScope.pledge = data.pledge;
						  if ($rootScope.pledge && $rootScope.pledge.admins){
							  $rootScope.pledge.admins.forEach(function(admin){
								  if (admin.rippleAddress ==  $rootScope.loggedInAccount.publicKey){
									  $rootScope.admin = admin;
								  }
								  if (admin.signedPledge && !admin.valid){
									  accountServices.verifySignature(admin).then(function(sigg){
										  var inRayed = _.map($rootScope.pledge.admins, function(sigged) {
											  if (sigg.signature == sigged.signature){
												  sigged = sigg;
											  }
											  return sigged;
										  });
									  });
								  }
							  });
						  }
						  if ($rootScope.pledge.signatures && $rootScope.pledge.signatures[0]){
							  $rootScope.pledge.signatures.forEach(function(sig){
								  if (sig.signedPledge && !sig.valid){
									  accountServices.verifySignature(sig).then(function(sigg){
										  var inRayed = _.map($rootScope.pledge.signatures, function(sigged) {
											  if (sigg.signature == sigged.signature){
												  sigged = sigg;
											  }
											  return sigged;
										  });
									  });
								  }
							  });
						  }
						  $rootScope.socket.emit('readyRoom', {_id : $rootScope.pledge._id});
					  }
					  defer.resolve();
				  }).
				  error(function(data, status, headers, config) {

				  });
			  });
			  return defer.promise;
		  }]
	  }
    })

    .state('tab.pledger', {
	  url: '/pledger',
	  views: {
		  'tab-pledger': {
			  templateUrl: 'templates/tab-pledger.html',
			  controller: 'PledgerCtrl'
		  }
	  }
    })

    .state('tab.mypledges', {
	  url: '/mypledges/:name',
	  views: {
		  'tab-mypledges': {
			  templateUrl: 'templates/tab-mypledges.html',
			  controller: 'MyPledgesCtrl'
		  }
	  },
	  resolve: {
		getPledges: ['$q', '$stateParams', '$rootScope', 'accountServices', '$http', function($q, $stateParams, $rootScope, accountServices, $http){
			var deferr = $q.defer();
			$rootScope.loadingMyPledges = true;
			accountServices.getRippleTxtSettings().then(function(){
				if ($rootScope.loggedInAccount && $rootScope.loggedInAccount.publicKey){
					var poster = function(objectReqNew, defer){
						$http.post('https://invest.providence.solutions/mypledges', objectReqNew)
							.success(function(data, status, headers, config) {
								if (data && data.pledges){
									data.pledges = _.map(data.pledges, function(pledge){
										if (pledge.admins){
											pledge.admins.forEach(function(admin){
												if (admin.rippleName == $rootScope.loggedInAccount.rippleName){
													pledge.isAdmin = true;
												}
											});
										}
										return pledge;
									});
									$rootScope.pledges = data.pledges;
								}
								$rootScope.loadingMyPledges = false;
								defer.resolve(true);
							}).
							error(function(data, status, headers, config) {
								$rootScope.loadingMyPledges = false;
								defer.resolve(false);
							});
					}
					accountServices.noNetNeutrality({}, poster).then(function(){
						deferr.resolve();
					});
				}
			});
			return deferr;
		}]
	  },
	  onEnter: ['$rootScope', '$state', function($rootScope, $state) {
		  if (!$rootScope.loggedInAccount || !$rootScope.loggedInAccount.publicKey){
			  $state.go('tab.pledger');
		  }
	  }]
    })

  $urlRouterProvider.otherwise('/tab/pledger');

}]);

