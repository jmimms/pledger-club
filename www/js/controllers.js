angular.module('starter.controllers', ['emoticonizeFilter'])

.filter('unique', function() {
	return function(collection, keyname) {
		var output = [],
			keys = [];

		angular.forEach(collection, function(item) {
			var key = item[keyname];
			if(keys.indexOf(key) === -1) {
				keys.push(key);
				output.push(item);
			}
		});

		return output;
	};
})

.directive('youtube', ['$sce', function($sce) {
	return {
		restrict: 'EA',
		scope: { code:'=' },
		replace: true,
		template: '<div style="height:400px;"><iframe style="overflow:hidden;height:100%;width:100%" width="100%" height="100%" src="{{url}}" frameborder="0" allowfullscreen></iframe></div>',
		link: function (scope) {
			scope.$watch('code', function (newVal) {
				if (newVal) {
					scope.url = $sce.trustAsResourceUrl("https://www.youtube.com/embed/" + newVal);
				}
			});
		}
	};
}])

.directive('keepScroll', ['$rootScope', '$location', '$anchorScroll', function($rootScope, $location, $anchorScroll){
	return {
		controller : function($scope){
			var element = null;

			this.setElement = function(el){
				element = el;
			}

			this.addItem = function(item){
				if ($rootScope.waypoints && $rootScope.waypoints.waypoints && $rootScope.waypoints.position && $rootScope.waypoints.waypoints.position.up){
					element.scrollTop = (element.scrollTop+item.clientHeight+1);
				}
				else {
					$("#scrolled").scrollTop($("#scrolled")[0].scrollHeight);
				}
			};
		},
		link : function(scope,el,attr, ctrl) {
			ctrl.setElement(el[0]);
		}
	};
}])

.directive('scrollItem', function(){
	return{
		require : "^keepScroll",
		link : function(scope, el, att, scrCtrl){
			scrCtrl.addItem(el[0]);
		}
	}
})

.directive('signature', ['$rootScope', function ($rootScope){
	return {
		template: '<canvas id="canvas" height="200" style="border: 1px solid #ccc;text-align:center;"></canvas>',
		restrict: 'E',
		link: function (scope, element, attrs) {
			var canvas = document.querySelector("canvas");
			$rootScope.signatures.signaturePad = new SignaturePad(canvas, {
				penColor: "#14274d",
				maxWidth: 2
			});
		}
	};
}])

.directive('twitter', [
	function() {
		return {
			link: function(scope, element, attr) {
				setTimeout(function() {
					if (window.twttr){
						window.twttr.widgets.createShareButton(
							attr.url,
							element[0],
							function(el) {}, {
								text: attr.text,
								size: attr.size,
								count: 'none',
								hashtags: attr.hashtags,
								via: attr.via
							}
						);
					}
				});
			}
		}
	}
])

.controller('CardsCtrl', ['$scope', '$ionicSwipeCardDelegate', '$state', '$rootScope', function($scope, $ionicSwipeCardDelegate, $state, $rootScope) {
	var cardTypes = [
		{
			title: 'pledger',
			image: 'https://d1pzmogk9nquph.cloudfront.net/img/bevelBlog.png'
		},
		{
			title: 'pledge',
			image: 'https://d1pzmogk9nquph.cloudfront.net/img/bevelBlog.png'
		}
	];

	function hideCards(){
		if ($('#start-card').length){
			$('#start-card').hide();
		}
		if ($('.swipe-card').length){
			$('.swipe-card').hide();
		}
	}

	var index = 0;

	$rootScope.$on('$stateChangeStart',
	function(event, toState, toParams, fromState, fromParams){
		var counter = 0;
		if (!$rootScope.stated){
			cardTypes.forEach(function(card){
				if (toState.name == ('tab.' + card.title)){
					index = counter;
					$scope.starterSrc = card;
				}
				counter += 1;
			});
			$scope.cards = cardTypes[index];
		}

		$rootScope.stated = toState.name;
	});

	$scope.cardSwiped = function(indexed) {
		hideCards();
		if (index == (cardTypes.length - 1)){
			index = 0;
		}
		else {
			index += 1;
		}
		$state.go('tab.' + cardTypes[index].title);
	};
}])

.controller('CardCtrl', ['$scope', '$ionicSwipeCardDelegate', function($scope, $ionicSwipeCardDelegate) {
}])

.controller('PledgeCtrl', ['$scope', '$http', '$rootScope', 'accountServices', '$q', '$ionicPopup', '$routeParams', '$stateParams', '$localForage', function($scope, $http, $rootScope, accountServices, $q, $ionicPopup, $routeParams, $stateParams, $localForage) {
	$scope.data = {};
	$scope.data.info = {};
	$scope.data.info.backup = true;
	$scope.data.date = new Date();
	$scope.data.index = 20;
	$scope.data.noMore = true;
	$rootScope.sectioned = 'pledge';
	accountServices.twitter();

	$scope.provablySigned = function(signature){
		$scope.data.selectedSignature = signature;
		var myPopup = $ionicPopup.show({
			templateUrl: 'templates/provably-signed.html',
			title: '<span class="providence-primary ionHead">Provably Signed</span>',
			scope: $scope,
			buttons: [
				{
					text: '<span class="providence-primary smallButton">Close</span>',
					type: 'marketing-secondary-bg'
				}
			]
		});
	}

	$scope.getSignatures = function(){
		$http.get('https://invest.providence.solutions/moresignatures?_id=' + $rootScope.pledge._id.toString() + '?index=' + $scope.data.index.toString()).
			success(function(data, status, headers, config) {
				if (data.signatures && data.signatures[0]){
					if (data.signatures.length >= 20){
						$scope.data.noMore = false;
					}
					else {
						$scope.data.noMore = true;
					}
					$scope.data.index += data.signatures.length;
					data.signatures.forEach(function(sig){
						var inRay = _.find($rootScope.pledge.signatures, function(sigged) {return sig.signature == sigged.signature;});
						if (!inRay){
							if (sig.signedPledge && !sig.valid){
								accountServices.verifySignature(sig).then(function(sigg){
									var inRayed = _.find($rootScope.pledge.signatures, function(sigged) {return sigg.signature == sigged.signature;});
									if (!inRayed){
										$rootScope.pledge.signatures.push(sigg);
									}
								});
							}
							else {
								$rootScope.pledge.signatures.push(sig);
							}
						}
					});
				}
				$rootScope.pledge.signatureLength = data.total;
			}).
			error(function(data, status, headers, config) {

			});
	}

	$scope.signSend = function(){
		var defer = $q.defer();
		$scope.data.info.signature = $rootScope.signatures.signaturePad.toDataURL();
		if ($scope.data.info.signature && $scope.data.info.name  && accountServices.validateEmail($scope.data.info.email)){
			$scope.data.info.rippleAddress = $rootScope.loggedInAccount.publicKey;
			$scope.data.info.rippleName = $rootScope.loggedInAccount.rippleName;
			$scope.data.info._id = $rootScope.pledge._id;
			var today = new Date();
			$scope.data.info.reportedDate = today.toString();
			if ($rootScope.loggedInAccount.publicKey){
				$scope.data.info.signedPledge = ripple.Message.signMessage($scope.data.info.reportedDate + $rootScope.pledge.pledge + $scope.data.info.signature, $rootScope.loggedInAccount.secretKey, $scope.data.info.rippleAddress);
			}
			var poster = function(){
				$http.post('https://invest.providence.solutions/signPledge', angular.copy($scope.data.info)).
					success(function(data, status, headers, config) {
						$rootScope.pledge.signatureLength = data.total;
						$localForage.getItem('profile').then(function(data) {
							if (data){
								var datum = JSON.parse(data);
								if (datum && datum[0]){
									var ined = false;
									datum = _.map(datum, function(dat){
										if (dat.rippleName == $rootScope.loggedInAccount.rippleName){
											$rootScope.info.name = dat.name;
											$rootScope.info.email = dat.email;
											$scope.data.info.name = dat.name;
											$scope.data.info.email = dat.email;
											ined = true;
										}
										return dat;
									});
									if (!ined){
										datum.push({rippleName: $rootScope.loggedInAccount.rippleName, publicKey: $rootScope.loggedInAccount.publicKey, name: $rootScope.info.name, email: $rootScope.info.email});
									}
									$localForage.setItem('profile', JSON.stringify(datum)).then(function(data) {

									});
								}
							}
							else {
								var packet = JSON.stringify([{rippleName: $rootScope.loggedInAccount.rippleName, publicKey: $rootScope.loggedInAccount.publicKey, name: $rootScope.info.name, email: $rootScope.info.email}]);
								$localForage.setItem('profile', packet).then(function(data) {

								});
							}
						});
						$scope.data.info.signature = '';
						$scope.data.info.rippleAddress = '';
						$scope.data.info.rippleName = '';
						$scope.data.info.signedPledge = '';
						$scope.data.info.backupHash = '';
						$scope.data.info.reportedDate = '';
						$scope.data.info.backup = true;
						defer.resolve(true);
					}).
					error(function(data, status, headers, config) {
						defer.resolve(false);
					});
			}
			var rippleFunc = function(){
				var amount = ripple.Amount.from_human('0.1XRP');
				$rootScope.remote.setSecret($rootScope.loggedInAccount.publicKey, $rootScope.loggedInAccount.secretKey);

				var transaction = $rootScope.remote.createTransaction('Payment', {
					account: $rootScope.loggedInAccount.publicKey,
					destination: $rootScope.receiveAddress,
					amount: amount
				});

				transaction.addMemo('Pledger Club', JSON.stringify({type : 'pledge', domain : 'pledger.club', id : $rootScope.pledge._id.toString(), role : 'pledger', noHandSignVerifiablePledge : ripple.Message.signMessage($scope.data.info.reportedDate + $scope.data.info.pledge, $rootScope.loggedInAccount.secretKey, $rootScope.info.rippleAddress), signedPledge : $scope.data.info.signedPledge, date : $scope.data.info.reportedDate.toString()}));

				transaction.addMemo('request', '', $rootScope.loggedInAccount.secretKey, $scope.data.info.rippleAddress);

				transaction.submit(function(err, res) {
					if (!err && res && res.tx_json && res.tx_json.hash){
						$scope.data.info.signedDateTime = ripple.Message.signMessage(res.tx_json.date.toString(), $rootScope.loggedInAccount.secretKey, $scope.data.info.rippleAddress);
						$scope.data.info.backupHash = res.tx_json.hash;
						$scope.data.info.hash = res.tx_json.hash;
						$localForage.getItem('lastTransactions').then(function(data) {
							var transactionList = JSON.parse(data);
							var inList;
							if (transactionList && transactionList[0]){
								transactionList = _.map(transactionList, function(item){if (item.domain == $rootScope.domain){inList = true; item = {hash : res.tx_json.hash, dateTime: res.tx_json.date.toString(), rippleName: $rootScope.loggedInAccount.rippleName, rippleAddress: $rootScope.loggedInAccount.publicKey}} return item;});
							}
							if (!inList){
								transactionList.push({domain : $rootScope.domain, hash : res.tx_json.hash, dateTime: res.tx_json.date.toString(), rippleName: $rootScope.loggedInAccount.rippleName, rippleAddress: $rootScope.loggedInAccount.publicKey});
							}
							$localForage.setItem('lastTransactions', JSON.stringify(transactionList)).then(function() {

							});
						});
						poster();
					}
				});
			}
			if ($rootScope.loggedInAccount.rippleName){
				if ($rootScope.remote.isConnected()){
					rippleFunc();
				}
				else {
					$rootScope.remote.connect(function() {
						rippleFunc();
					});
				}
			}
			else {
				poster();
			}
		}
		else {
			defer.resolve(false);
		}
		return defer.promise;
	}

	$scope.signActivate = function(){
		$scope.data.info.name = $rootScope.info.name;
		$scope.data.info.email = $rootScope.info.email;
		var myPopup = $ionicPopup.show({
			template: '<span class="providence-primary" style="position: absolute;text-align: center;left: 50%;top: 35px;margin-left: -7px;" ng-if="loggedInAccount.signing"><i class="icon ion-loading-c" style="height: 14px;width: 13px;"></i></span><style>.popup { width:321px !important; } .popup-body { padding-bottom: 1px !important; }</style><form name="signForm"><label class="item item-input"><input type="text" placeholder="Name" ng-model="data.info.name" required></label><label class="item item-input"><input type="email" placeholder="Email" ng-model="data.info.email" required></label><signature data-tap-disabled="true"></signature><li class="item item-checkbox providence-primary" ng-if="loggedInAccount.rippleName" style="font-size:18px;"><label class="checkbox"><input type="checkbox" ng-model="data.info.backup" checked></label>Save in ledger</li></form>',
			title: '<span class="providence-primary ionHead">Sign Our Pledge</span>',
			scope: $scope,
			buttons: [
				{
					text: '<span class="providence-primary smallButton">Cancel</span>',
					type: 'marketing-secondary-bg'
				},
				{
					text: '<span class="providence-primary smallButton">Sign</span>',
					type: 'marketing-primary-bg',
					onTap: function(e) {
						e.preventDefault();
						if (!$rootScope.loggedInAccount.signing && $rootScope.loggedInAccount.rippleName){
							$rootScope.loggedInAccount.signing = true;
							$scope.signSend().then(function(val){
								$rootScope.loggedInAccount.signing = false;
								if (val){
									myPopup.close();
								}
							});
						}
						else {
							$rootScope.loggedInAccount.signing = false;
							$scope.signSend().then(function(val){
								if (val){
									myPopup.close();
								}
							});
						}
					}
				}
			]
		});
	}

	$scope.$on("$destroy", function() {
		$rootScope.pledge = {};
		$rootScope.socket.emit('leaveRoom', {_id : $rootScope.pledge._id});
	});
}])

.controller('PledgerCtrl', ['$scope', '$http', '$rootScope', 'accountServices', '$q', '$ionicPopup', function($scope, $http, $rootScope, accountServices, $q, $ionicPopup) {
	$scope.data = {};
	$rootScope.sectioned = 'home';
	$scope.data.date = new Date();
	accountServices.twitter();
}])

.controller('MyPledgesCtrl', ['$scope', '$http', '$rootScope', 'accountServices', '$q', '$ionicPopup', '$routeParams', '$stateParams', '$localForage', '$location', function($scope, $http, $rootScope, accountServices, $q, $ionicPopup, $routeParams, $stateParams, $localForage, $location) {
	$scope.data = {};
	$scope.data.date = new Date();
	$scope.data.info = {};
	$scope.data.section = 'all';
	accountServices.twitter();
	$scope.switchSection = function(section){
		$scope.data.section = section;
	}
	$rootScope.sectioned = 'myPledges';
	$scope.navigate = function(id){
		$location.path('tab/pledge/' + id.toString());
	}
}]);
